package action;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import kit.Kit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IndexController extends Controller {
	public void index() {
	}

	@ActionKey("/deptList")
	public void deptList() {
		Map retMap=new HashMap();
		retMap.put("is",true);
		retMap.put("msg","查询成功！");
		try {
			List<Record> list=Db.find("SELECT * FROM sys_dept");
			Kit.setLayTree(retMap,list);

			int type=getParaToInt("type");
			if(type%2==0){
				for (Record r :list){
					r.set("testfield",Math.random());
				}
			}else{
				for (Record r :list){
					r.set("testfield2",Math.random());
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			retMap.put("is",false);
			retMap.put("msg","查询失败！");
		}
		renderJson(retMap);
	}
}


